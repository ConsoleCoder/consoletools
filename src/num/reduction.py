#!/usr/bin/python3


import common_factor

def reduction(numerator, denominator) -> str:
    max_fac = common_factor.max_common_factor(numerator, denominator)
    if max_fac == 1:
        return f"{numerator}/{denominator}"
    if max_fac == numerator:
        return f"1/{denominator // numerator}"
    if max_fac == denominator:
        return str(numerator // denominator)
    return f"{numerator // max_fac}/{denominator // max_fac}"

if __name__ == "__main__":
    while True:
        numerator, denominator = [int(i) for i in input("Type the first fraction (n/n): ").split("/")]
        print("Result:", reduction(numerator, denominator))
        if input("Do you want to continue? (Y/n) ").lower() == 'n':
            break
