#!/usr/bin/python3

import search_factor
import utils

def common_factor(a: int, b: int):
    if a + 1 == b or a - 1 == b:
        return {1}
    if utils.is_prime(a) or utils.is_prime(b):
        return {1}
    a_fac = set(search_factor.search(a))
    b_fac = set(search_factor.search(b))
    return a_fac & b_fac

def max_common_factor(a: int, b: int) -> int:
    if a % b == 0:
        return b
    if b % a == 0:
        return a
    if utils.is_prime(a) or utils.is_prime(b):
        return 1
    return max(common_factor(a, b))

if __name__ == "__main__":
    option = input("Type the first num (enter e to end): ")
    while option != "e":
        print("Their common factor:", common_factor(int(option), int(input("Type the second num: "))))
        print("Their max common factor:", max_common_factor(int(option), int(input("Type the second num: "))))
        print("-" * 15)
        option = input("Type the first num (enter e to end): ")
