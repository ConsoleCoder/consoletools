#!/usr/bin/python3

def search(num: int):
    for i in range(1, num + 1):
        if num % i == 0:
            yield i


if __name__ == "__main__":
    target = int(input())
    print(', '.join(str(i) for i in search(target)))
