#!/usr/bin/python3

import least_common_multiple
import argparse

def get_general_score(numerator, denominator, numerator1, denominator1) -> tuple[str, str]:
    denominator_mul = least_common_multiple.least_common_multiple(denominator, denominator1)
    coefficient = denominator_mul // denominator
    coefficient1 = denominator_mul // denominator1
    return (f"{numerator * coefficient}/{denominator * coefficient}",
            f"{numerator1 * coefficient1}/{denominator1 * coefficient1}")

def get_general_score_with_common_denominator(numerator, denominator, numerator1, denominator1, common_denominator):
    coefficient = common_denominator // denominator
    coefficient1 = common_denominator // denominator1
    return (f"{numerator * coefficient}/{denominator * coefficient}",
            f"{numerator1 * coefficient1}/{denominator1 * coefficient1}")

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-w', "--with_common_denominator", action="store_true")
    args = argparser.parse_args()
    while True:
        numerator, denominator = [int(i) for i in input("Type the first fraction (n/n): ").split("/")]
        numerator1, denominator1 = [int(i) for i in input("Type the second fraction: ").split("/")]
        if args.with_common_denominator:
            common_denominator = int(input("Type the common denominator: "))
            print("Result:", get_general_score_with_common_denominator(numerator, denominator,
                                                                       numerator1, denominator1, common_denominator))
        else:
            print("Result:", get_general_score(numerator, denominator, numerator1, denominator1))
        if input("Do you want to continue? (Y/n) ").lower() == 'n':
            break
