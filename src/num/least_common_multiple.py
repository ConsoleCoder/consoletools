#!/usr/bin/python3

import common_factor
import utils

def least_common_multiple(a: int, b: int):
    if utils.is_prime(a) or utils.is_prime(b):
        return a * b
    if a + 1 == b:
        return a * b
    if a % b == 0:
        return a
    if b % a == 0:
        return b

    max_fac = common_factor.max_common_factor(a, b)
    return (a // max_fac) * (b // max_fac) * max_fac

if __name__ == "__main__":
    option = input("Type the first num (enter e to end): ")
    while option != "e":
        print("Their least common multiple:", least_common_multiple(int(option), int(input("Type the second num: "))))
        print("-" * 15)
        option = input("Type the first num (enter e to end): ")