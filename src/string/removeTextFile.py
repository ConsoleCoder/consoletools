#!/usr/bin/python3

import argparse
import removeText

def handleFile(inp: str, out: str, delete: str, mode: str):
    with open(inp, 'r', encoding='utf-8') as f:
        with open(out, 'w', encoding='utf-8') as result:
            while (line := f.readline()):
                result.write(removeText.handleRemove(line, delete, mode))

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description="Use \"removeText\" to process a file")
    argparser.add_argument("filename", type=str, help="The file to process")
    argparser.add_argument("-o", "--output", required=False, type=str, help="The target file")
    args = argparser.parse_args()

    mode = input("Delete the scope / location of the text? (L(eft), R(ight) or A(ll)) ")
    text = input("The text you want to delete? ")

    filename: str
    if args.output:
        filename = args.output
    else:
        if '.' in args.filename:
            filename = '.'.join(args.filename.split(".")[:-2]) + "_handled." + args.filename.split(".")[-1]
        else:
            filename = args.filename + "_handled"
    handleFile(args.filename, filename, text, mode)
