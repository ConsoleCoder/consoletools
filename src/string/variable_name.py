#!/usr/bin/python3

from translate import Translator


def handle(target: str, is_const: bool):
    if const:
        return (target.lower().lstrip("the ").replace(' ', '_').upper())
    else:
        mode = int(input("Naming style? (1. xxx_xxx, 2. xxxXxx, 3. XxxXxx) "))
        if mode == 1:
            return (target.lower().lstrip("the ").replace(' ', '_'))
        elif mode == 2:
            first = result.lower().lstrip("the ")
            ret = ''
            for i in range(len(first)):
                if first[i - 1] == ' ':
                    ret += (first[i].upper())
                    continue
                elif first[i] == ' ':
                    continue
                ret += (first[i])
            return ret
        elif mode == 3:
            first = result.lower().lstrip("the ")
            ret = first[0].upper()
            for i in range(1, len(first)):
                if first[i - 1] == ' ':
                    ret += (first[i].upper())
                    continue
                elif first[i] == ' ':
                    continue
                ret += (first[i])
            return ret
    raise ValueError

if __name__ == "__main__":
    translator = Translator("en", input("Enter the code for the language you are going to enter: "))
    result = translator.translate(input("Use a noun to describe your variable? "))
    const = input("Is it a constant? y/N ").lower() == "y"

    print(handle(result, const))
