#!/usr/bin/python3

def handleRemove(target: str, delete: str, mode: str) -> str:
    mode = mode.lower()
    if mode == 'l' or mode == 'left':
        return (target.lstrip(delete))
    elif mode == 'r' or mode == 'right':
        return (target.rstrip(delete))
    elif mode == 'a' or mode == 'all':
        return (target.replace(delete, ''))
    return target

if __name__ == "__main__":
    print("Please enter data (enter e to end): ")
    data: list[str] = []
    line = input()
    while line != 'e':
        data.append(line)
        line = input()

    pos = input("Delete the scope / location of the text? (L(eft), R(ight) or A(ll)) ")
    pos = pos.lower()
    text = input("The text you want to delete? ")

    for i in data:
        print(handleRemove(i, text, pos))
